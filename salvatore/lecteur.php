<?php

/*
	This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

	Salvatore is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Trad-Lang is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Trad-Lang; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Copyright 2003-2020
		Florent Jugla <florent.jugla@eledo.com>,
		Philippe Riviere <fil@rezo.net>,
		Chryjs <chryjs!@!free!.!fr>,
		kent1 <kent1@arscenic.info>
		Cerdic <cedric@yterium.com>
*/


include_spip('base/abstract_sql');
include_spip('inc/charsets');
include_spip('inc/filtres');
include_spip('inc/texte');
include_spip('inc/xml');
include_spip('inc/lang_liste');
include_spip('inc/session');

/**
 * @param array $liste_sources
 * @param bool $force_reload
 * @param string $dir_modules
 * @throws Exception
 */
function salvatore_lire($liste_sources, $force_reload = false, $dir_modules = null) {
	include_spip('inc/salvatore');
	salvatore_init();

	// on va modifier a la base, il faut qu'elle soit a jour
	salvatore_verifier_base_upgradee();

	if (is_null($dir_modules)) {
		$dir_modules = _DIR_SALVATORE_MODULES;
	}
	salvatore_check_dir($dir_modules);
	$refresh_time = time() - _SALVATORE_LECTEUR_REFRESH_DELAY;

	$tradlang_verifier_langue_base = charger_fonction('tradlang_verifier_langue_base', 'inc', true);
	$tradlang_verifier_bilans = charger_fonction('tradlang_verifier_bilans', 'inc', true);
	$invalider = false;

	foreach ($liste_sources as $source) {
		salvatore_log("\n<info>--- Module " . $source['module'] . ' | ' . $source['dir_module'] . ' | ' . $source['url'] . '</info>');

		$module = $source['module'];
		$dir_module = $dir_modules . $source['dir_module'];

		if ($autre_gestionnaire = salvatore_verifier_gestionnaire_traduction($dir_module, $module)) {
			salvatore_fail("[Lecteur] Erreur sur $module", "Erreur : import impossible, le fichier est traduit autre part : $autre_gestionnaire\n");
		}

		/**
		 * on doit absolument charger la langue principale en premier (a cause des MD5)
		 */
		$fichier_lang_principal = $dir_module . '/' . $module . '_' . $source['lang'] . '.php';
		$liste_fichiers_lang = glob($dir_module . '/' . $module . '_*.php');
		if (!in_array($fichier_lang_principal, $liste_fichiers_lang)) {
			salvatore_fail("[Lecteur] Erreur sur $module", "|-- Pas de fichier lang principal $fichier_lang_principal : import impossible pour ce module");
		}

		// pour la suite, on enleve la langue principale de la liste des fichiers
		$liste_fichiers_lang = array_diff($liste_fichiers_lang, [$fichier_lang_principal]);

		/**
		 * On regarde quelle est la date de dernière modification du fichier de langue principale
		 */
		$last_update = filemtime($fichier_lang_principal);
		$type_export = salvatore_retrouver_type_export($fichier_lang_principal);

		if ($row_module = salvatore_retrouver_tradlang_module($dir_module, $module)) {
			$id_tradlang_module = (int) $row_module['id_tradlang_module'];
			salvatore_log("Module en base #$id_tradlang_module");
			/**
			 * Si la langue mere a changée, on la modifie
			 */
			if ($row_module['lang_mere'] !== $source['lang']) {
				sql_updateq('spip_tradlang_modules', ['lang_mere' => $source['lang']], 'id_tradlang_module=' . (int) $id_tradlang_module);
				salvatore_log('lang_mere mise a jour : ' . $row_module['lang_mere'] . ' => ' . $source['lang']);
				$last_update = time();
				$row_module['lang_mere'] = $source['lang'];
			}
			/**
			 * Si le type d’export de fichier a changé, on le modifie
			 */
			if ($row_module['type_export'] !== $type_export) {
				sql_updateq('spip_tradlang_modules', ['type_export' => $type_export], 'id_tradlang_module=' . (int) $id_tradlang_module);
				salvatore_log('type_export mise a jour : ' . $row_module['type_export'] . ' => ' . $type_export);
				$last_update = time();
				$row_module['type_export'] = $type_export;
			}
			/**
			 * Si le dir_module a change, on le met a jour
			 */
			if ($row_module['dir_module'] !== $source['dir_module']) {
				sql_updateq('spip_tradlang_modules', ['dir_module' => $source['dir_module']], 'id_tradlang_module=' . (int) $id_tradlang_module);
				salvatore_log('dir_module mis a jour : ' . $row_module['dir_module'] . ' => ' . $source['dir_module']);
				$last_update = time();
				$row_module['dir_module'] = $source['dir_module'];
			}
			/**
			 * On met le titre a jour si jamais il a change (ou si on a change de facon de le calculer)
			 */
			if (($t = calculer_nom_module($source['module'], $source['dir_module'])) !== $row_module['nom_mod']) {
				sql_updateq('spip_tradlang_modules', ['nom_mod' => $t], 'id_tradlang_module=' . (int) $id_tradlang_module);
				salvatore_log('nom_mod mis a jour : ' . $row_module['nom_mod'] . ' => ' . $t);
				$last_update = time();
				$row_module['nom_mod'] = $t;
			}
		}

		$new_module = false;
		$langues_a_jour = [];

		if (!$row_module || $force_reload || $last_update > $refresh_time) {
			$priorite = '';
			if (defined('_TRAD_PRIORITE_DEFAUT')) {
				$priorite = _TRAD_PRIORITE_DEFAUT;
			}

			/**
			 * Si le module n'existe pas... on le crée
			 */
			if (!$row_module || (!$id_tradlang_module = (int) $row_module['id_tradlang_module'])) {
				$insert = [
					'module' => $source['module'],
					'dir_module' => $source['dir_module'],
					'nom_mod' => calculer_nom_module($source['module'], $source['dir_module']),
					'lang_prefix' => $source['module'],
					'lang_mere' => $source['lang'],
					'type_export' => $type_export,
					'priorite' => $priorite,
				];
				$id_tradlang_module = sql_insertq('spip_tradlang_modules', $insert);
				/**
				 * Si insertion echoue on fail
				 */
				if (!(int) $id_tradlang_module) {
					salvatore_fail("[Lecteur] Erreur sur $module", 'Echec insertion dans spip_tradlang_modules ' . json_encode($insert, JSON_THROW_ON_ERROR));
				} else {
					salvatore_log("Insertion en base #$id_tradlang_module");
				}
				$new_module = true;
			}
			$force_reload = true;
		} else {
			// Pas de mise a jour recente du fichier maitre deja en base
			salvatore_log("On ne modifie rien : fichier original $fichier_lang_principal inchangé depuis " . date('Y-m-d H:i:s', $last_update));
			$id_tradlang_module = (int) $row_module['id_tradlang_module'];

			/**
			 * Le fichier d'origine n'a pas été modifié
			 * Mais on a peut être de nouvelles langues
			 */
			$langues_en_base = sql_allfetsel('DISTINCT lang', 'spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module);
			$langues_en_base = array_column($langues_en_base, 'lang');

			$langues_a_ajouter = [];
			foreach ($liste_fichiers_lang as $fichier_lang) {
				$lang = salvatore_get_lang_from($module, $fichier_lang);
				if (!in_array($lang, $langues_en_base)) {
					$langues_a_ajouter[] = ['lang' => $lang, 'fichier' => $fichier_lang];
				} else {
					// inutile de regarder ce fichier
					$langues_a_jour[] = $lang;
				}
			}

			$liste_fichiers_lang = [];
			if ($langues_a_ajouter) {
				salvatore_log('On a ' . count($langues_a_ajouter) . ' nouvelle(s) langue(s) à insérer (' . count($langues_en_base) . ' langue(s) an base)');
				$liste_fichiers_lang = array_column($langues_a_ajouter, 'fichier');
			}
		}

		// traiter les fichiers lang
		if (count($liste_fichiers_lang) || $force_reload) {
			// on commence par la langue mere
			$liste_md5_master = [];
			$modifs_master = salvatore_importer_module_langue($id_tradlang_module, $source, $fichier_lang_principal, true, $new_module, $liste_md5_master);

			// et on fait les autres langues
			foreach ($liste_fichiers_lang as $fichier_lang) {
				salvatore_importer_module_langue($id_tradlang_module, $source, $fichier_lang, false, $new_module, $liste_md5_master);

				$lang = salvatore_get_lang_from($module, $fichier_lang);
				if ($modifs_master > 0) {
					if ($tradlang_verifier_langue_base) {
						$tradlang_verifier_langue_base($id_tradlang_module, $lang);
						salvatore_log('|-- Synchro de la langue ' . $lang . ' pour le module ' . $source['module']);
					} else {
						salvatore_log("<error>|-- Pas de Fonction de synchro inexistante pour synchroniser lang $lang</error>");
					}
				}
				$langues_a_jour[] = $lang;
			}

			/**
			 * On s'occupe des langues en base sans fichier
			 * s'il y a eu au moins une modif et que l'on peut faire la synchro
			 */
			if ($modifs_master > 0 && $tradlang_verifier_langue_base) {
				$langues_en_base = sql_allfetsel('DISTINCT lang', 'spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module);
				$langues_en_base = array_column($langues_en_base, 'lang');

				if ($langues_pas_a_jour = array_diff($langues_en_base, $langues_a_jour)) {
					foreach ($langues_pas_a_jour as $langue_todo) {
						$tradlang_verifier_langue_base($id_tradlang_module, $langue_todo);
						salvatore_log("|-- Synchro de la langue non exportée en fichier $langue_todo pour le module $module");
					}
				}
			}

			$invalider = true;
			salvatore_log('|');
			unset($langues_a_jour, $langues_pas_a_jour);
		}

		// Mise à jour des bilans
		if ($tradlang_verifier_bilans) {
			salvatore_log("Création ou MAJ des bilans du module #$id_tradlang_module $module");
			$tradlang_verifier_bilans($id_tradlang_module, $source['lang'], false);
			salvatore_log('-');
		}
	}

	if ($invalider) {
		include_spip('inc/invalideur');
		suivre_invalideur('1');
	}
}

/** @return array{commentaires: array<string,string>, chaines: array<string,string>} */
function salvatore_importer_module_langue_chaines(string $fichier_lang): ?array {
	// type spip ou spip5 ?
	$type_export = salvatore_retrouver_type_export($fichier_lang);
	// charger le fichier et ses commentaires
	$chaines = null;
	if ($type_export === 'spip') {
		$commentaires = salvatore_charger_commentaires_fichier_langue_spip($fichier_lang);
		$chaines = salvatore_importer_module_langue_chaines_spip($fichier_lang);
	} elseif ($type_export === 'spip5') {
		$commentaires = salvatore_charger_commentaires_fichier_langue_spip5($fichier_lang);
		$chaines = salvatore_importer_module_langue_chaines_spip5($fichier_lang);
	}
	if ($chaines === null || !is_array($chaines)) {
		return null;
	}
	return [
		'commentaires' => $commentaires,
		'chaines' => $chaines,
	];
}

function salvatore_importer_module_langue_chaines_spip(string $fichier_lang): ?array {
	$idx = $GLOBALS['idx_lang'] = 'i18n_' . crc32($fichier_lang) . '_tmp';
	$GLOBALS[$idx] = null;
	include $fichier_lang;
	$chaines = $GLOBALS[$idx];  // on a vu certains fichiers faire des betises et modifier idx_lang
	return $chaines;
}

function salvatore_importer_module_langue_chaines_spip5(string $fichier_lang): ?array {
	$chaines = include $fichier_lang;
	return is_array($chaines) ? $chaines : null;
}

/**
 * Import d'un fichier de langue dans la base
 *
 * @param int $id_tradlang_module
 * @param array $source
 *   tableau decrivant le module extrait du fichier traductions
 * @param string $fichier_lang
 *   chemin vers le fichier de langue
 * @param bool $is_master
 *   true signifie que c'est la langue originale
 * @param bool $new_module
 *   true signifie qu'on est en train d'importer un nouveau module
 * @param array $liste_md5_master
 * @return string|false
 */
function salvatore_importer_module_langue($id_tradlang_module, $source, $fichier_lang, $is_master, $new_module, &$liste_md5_master) {
	salvatore_log("!\n+ Import de $fichier_lang\n");
	$module = $source['module'];
	$lang = salvatore_get_lang_from($module, $fichier_lang);
	$desc = salvatore_importer_module_langue_chaines($fichier_lang);

	if ($desc === null) {
		$erreur = "Erreur, fichier $fichier_lang mal forme";
		salvatore_log("<error>$erreur</error>");
		salvatore_envoyer_mail("[Lecteur] Erreur sur $module", $erreur);
		return false;
	}
	$commentaires = $desc['commentaires'];
	$chaines = $desc['chaines'];

	/**
	 * Nettoyer le contenu de ses <MODIF>,<NEW> et <PLUS_UTILISE>
	 * Ces chaines sont utilisées comme statut
	 */
	$status = [];

	foreach ($chaines as $id => $chaine) {
		if ($is_master) {
			$status[$id] = 'OK';
		} else {
			if (preg_match(',^<(MODIF|NEW|PLUS_UTILISE)>,US', $chaine, $r)) {
				$chaines[$id] = preg_replace(',^(<(MODIF|NEW|PLUS_UTILISE)>)+,US', '', $chaine);
				$status[$id] = $r[1];
			} else {
				$status[$id] = 'OK';
			}
		}
	}

	$ajoutees = $inchangees = $supprimees = $modifiees = $ignorees = $recuperees = 0;

	if (array_key_exists($lang, $GLOBALS['codes_langues'])) {
		$statut_exclus = 'attic';
		$res = sql_select('id, str, md5', 'spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module . ' AND lang=' . sql_quote($lang) . ' AND statut!=' . sql_quote($statut_exclus));
		$nb = sql_count($res);
		if ($nb > 0) {
			salvatore_log("!-- Fichier de langue $lang du module $module deja inclus dans la base\n");
		}

		/**
		 * Si la langue est deja dans la base, on ne l'ecrase que s'il s'agit
		 * de la langue source
		 */
		if (!$nb || $is_master || $new_module) {
			// La liste de ce qui existe deja
			$existant = $str_existant = [];
			while ($row = sql_fetch($res)) {
				$existant[$row['id']] = $row['md5'];
				$str_existant[$row['id']] = $row['str'];
			}

			$bigwhere = 'id_tradlang_module=' . (int) $id_tradlang_module . ' AND lang=' . sql_quote($lang);

			include_spip('action/editer_tradlang');
			// Dans ce qui arrive, il y a 4 cas :
			$ids = array_unique(array_merge(array_keys($existant), array_keys($chaines)));
			foreach ($ids as $id) {
				$comm = $commentaires[$id] ?? '';

				/**
				 * 1. chaine neuve
				 */
				if (isset($chaines[$id]) && !isset($existant[$id])) {
					$md5 = null;
					if ($is_master) {
						$md5 = md5($chaines[$id]);
					} else {
						if (!isset($liste_md5_master[$id])) {
							salvatore_log("<info>!-- Chaine $id inconnue dans la langue principale</info>");
							$ignorees++;
						} else {
							$md5 = $liste_md5_master[$id];
						}
					}

					if ($md5) {
						$chaines[$id] = salvatore_nettoyer_chaine_langue($chaines[$id], $lang);

						/**
						 * Calcul du nouveau md5
						 */
						$md5 = md5($chaines[$id]);

						/**
						 * Si le commentaire est un statut et que l'on ne traite pas le fichier de langue mère
						 * On vire le commentaire et met son contenu comme statut
						 */
						if (in_array($comm, ['NEW', 'OK', 'MODIF', 'MODI']) && !$is_master) {
							if ($comm == 'MODI') {
								$comm = 'MODIF';
							}
							$status[$id] = $comm;
							$comm = '';
						} else {
							if ((strlen($comm) > 1) && preg_match('/(.*?)(NEW|OK|MODIF)(.*?)/', $comm, $matches)) {
								if (!$is_master) {
									$status[$id] = $matches[2];
								}
								$comm = preg_replace('/(NEW|OK|MODIF)/', '', $comm);
							}
						}

						/**
						 * On génère un titre
						 */
						$titre = $id . ' : ' . $source['module'] . ' - ' . $lang;

						$set = [
							'id_tradlang_module' => $id_tradlang_module,
							'titre' => $titre,
							'module' => $source['module'],
							'lang' => $lang,
							'id' => $id,
							'str' => $chaines[$id],
							'comm' => $comm,
							'md5' => $md5,
							'statut' => $status[$id]
						];
						$id_tradlang = sql_insertq('spip_tradlangs', $set);

						/**
						 * L'identifiant de la chaîne de langue a peut être déjà été utilisé puis mis au grenier
						 * On le récupère donc
						 */
						if (!$id_tradlang) {
							// mais il serait bien de pouvoir piquer une chaine attic du meme module meme si pas id_tradlang_module identique
							$tradlang = sql_fetsel('*', 'spip_tradlangs', 'id=' . sql_quote($id) . ' AND id_tradlang_module=' . (int) $id_tradlang_module . ' AND lang=' . sql_quote($lang) . ' AND statut=' . sql_quote('attic'));
							if ($tradlang && ($id_tradlang = (int) $tradlang['id_tradlang'])) {
								salvatore_log('<info>Recuperation chaine ' . $source['module'] . ":{$id}[{$lang}] de statut ATTIC</info>");
								sql_updateq('spip_tradlangs', $set, 'id_tradlang=' . (int) $id_tradlang);

								$trads = sql_allfetsel('id_tradlang', 'spip_tradlangs', 'id=' . sql_quote($id) . ' AND id_tradlang_module=' . (int) $id_tradlang_module . 'AND lang!=' . sql_quote($lang) . ' AND statut=' . sql_quote('attic'));
								$maj = ['statut' => 'MODIF'];
								foreach ($trads as $trad) {
									salvatore_log('Changement de la trad #' . $trad['id_tradlang'] . ' ATTIC => MODIF');
									sql_updateq('spip_tradlangs', $maj, 'id_tradlang=' . (int) $trad['id_tradlang']);
								}
								$recuperees++;
							} else {
								salvatore_fail('[Lecteur] Echec insertion', 'Echec insertion en base : ' . json_encode($set, JSON_THROW_ON_ERROR));
							}
						}

						/**
						 * Rechercher si on serait pas en train de copier une chaine existante
						 * (C'est un renommage)
						 *
						 * Si oui, on sélectionne toutes les occurences existantes dans les autres langues et on les duplique
						 */

						// si on a un contenu identique a une autre chaine du meme module
						// (renommage de l'id dans un meme fichier de langue)
						$where_identique = [
							'id_tradlang_module=' . (int) $id_tradlang_module,
							'lang=' . sql_quote($lang),
							'id!=' . sql_quote($id),
							'str=' . sql_quote($chaines[$id]),
						];
						$identique = sql_fetsel('id_tradlang_module, module, id', 'spip_tradlangs', $where_identique);

						// si on est sur la langue master on cherche si ce n'est pas un renommage/decoupe de module :
						// chercher une chaine avec meme id et meme contenu dans un autre module
						if (!$identique && $is_master) {
							$where_identique = [
								'id_tradlang_module!=' . (int) $id_tradlang_module,
								'lang=' . sql_quote($lang),
								'id=' . sql_quote($id),
								'str=' . sql_quote($chaines[$id]),
							];
							$identique = sql_fetsel('id_tradlang_module, module, id', 'spip_tradlangs', $where_identique);
						}


						if ($identique) {
							salvatore_log("La nouvelle chaine $id est une chaine dupliquée de " . $identique['module'] . ':' . $identique['id']);

							$deja_lang = sql_allfetsel('lang', 'spip_tradlangs', 'id=' . sql_quote($id) . ' AND id_tradlang_module=' . (int) $id_tradlang_module);
							$deja_lang = array_column($deja_lang, 'lang');
							// on ne prend pas les fausse trads dont str est identique $chaines[$id]
							$chaines_a_dupliquer = sql_allfetsel('*', 'spip_tradlangs', 'id=' . sql_quote($identique['id']) . ' AND id_tradlang_module=' . (int) $identique['id_tradlang_module'] . ' AND str!=' . sql_quote($chaines[$id]) . ' AND ' . sql_in('lang', $deja_lang, 'NOT'));
							foreach ($chaines_a_dupliquer as $chaine) {
								unset($chaine['id_tradlang']);
								unset($chaine['maj']);
								$chaine['id'] = $id;
								$chaine['id_tradlang_module'] = $id_tradlang_module;
								$chaine['module'] = $source['module'];
								$chaine['titre'] = $id . ' : ' . $source['module'] . ' - ' . $chaine['lang'];
								$chaine['md5'] = md5($chaine['str']);
								$chaine['date_modif'] = date('Y-m-d H:i:s');
								if ($chaine['statut'] == 'attic') {
									$chaine['statut'] = 'NEW';
								}
								$id_tradlang_new = sql_insertq('spip_tradlangs', $chaine);
								if (!$id_tradlang_new) {
									salvatore_fail('[Lecteur] Echec insertion', 'Echec insertion en base : ' . json_encode($chaine, JSON_THROW_ON_ERROR));
								}
								salvatore_log('Ajout de la version ' . $chaine['lang'] . ' - #' . $id_tradlang_new);
							}
						}
						$ajoutees++;
					}
				}


				/**
				 * 2. chaine existante
				 *
				 */
				elseif (isset($chaines[$id]) && isset($existant[$id])) {
					// * chaine existante
					// * identique ? => NOOP
					$chaines[$id] = salvatore_nettoyer_chaine_langue($chaines[$id], $lang);

					/**
					 * Calcul du nouveau md5
					 */
					$md5 = md5($chaines[$id]);
					if ($md5 === $existant[$id]) {
						$inchangees++;
					} else {
						// * modifiee ? => UPDATE
						salvatore_log("Chaine $id modifiee $md5 != " . $existant[$id]);

						// modifier la chaine
						$modifs = [
							'str' => $chaines[$id],
							'md5' => ($is_master ? $md5 : $existant[$id]),
							'statut' => ($is_master ? 'OK' : ''),
							'comm' => $comm
						];
						$id_tradlang = sql_getfetsel('id_tradlang', 'spip_tradlangs', "$bigwhere AND id = " . sql_quote($id));
						$test = tradlang_set($id_tradlang, $modifs);

						/**
						 * signaler le statut MODIF de ses traductions OK
						 * update des str de ses traductions NEW
						 */
						if ($is_master) {
							sql_updateq(
								'spip_tradlangs',
								['statut' => 'MODIF'],
								'id_tradlang_module=' . (int) $id_tradlang_module
								. ' AND id=' . sql_quote($id)
								. ' AND md5!=' . sql_quote($md5)
								. ' AND lang!=' . sql_quote($lang)
								. ' AND statut!=' . sql_quote('NEW')
							);
						}
						sql_updateq(
							'spip_tradlangs',
							['str' => $chaines[$id]],
							'id_tradlang_module=' . (int) $id_tradlang_module
							. ' AND id=' . sql_quote($id)
							. ' AND md5!=' . sql_quote($md5)
							. ' AND lang!=' . sql_quote($lang)
							. ' AND statut=' . sql_quote('NEW')
						);
						$modifiees++;
					}
				}

				/**
				 * 3. chaine supprimee
				 *
				 */
				elseif (!isset($chaines[$id]) && isset($existant[$id])) {
					// * chaine supprimee
					// mettre au grenier
					sql_updateq('spip_tradlangs', ['statut' => 'attic'], 'id_tradlang_module=' . (int) $id_tradlang_module . ' AND id=' . sql_quote($id));
					$supprimees++;
				}

				if ($is_master && isset($chaines[$id])) {
					$liste_md5_master[$id] = md5($chaines[$id]);
				}
			}
			salvatore_log('!-- module ' . $source['module'] . ", $lang : $modifiees modifiees, $ajoutees ajoutees, $supprimees supprimees, $recuperees recuperees, $ignorees ignorees, $inchangees inchangees");
		}
	}
	else {
		salvatore_log("<error>!-- Attention : La langue $lang n'existe pas dans les langues possibles - $module</error>");
	}

	unset($chaines, $GLOBALS[$GLOBALS['idx_lang']]);

	return $ajoutees + $supprimees + $modifiees;
}


/**
 * Chargement des commentaires de fichier de langue SPIP >= 5
 *
 * Le fichier est chargé en mode texte pour récupérer les commentaires dans lesquels sont situés les statuts
 *
 * @param string $fichier_lang Le chemin du fichier de langue
 * @return array $liste_trad Un tableau id/chaine
 */
function salvatore_charger_commentaires_fichier_langue_spip5($fichier_lang) {

	$contenu = file_get_contents($fichier_lang);
	$tokens = PhpToken::tokenize($contenu);

	$comments = [];

	// allons jusqu'au debut du tableau
	while (count($tokens)) {
		$token = array_shift($tokens);
		if ($token->is(T_RETURN)) {
			while ($token = array_shift($tokens)) {
				if ($token->is(T_WHITESPACE)) {
					continue;
				} elseif ($token->is('[')) {
					break 2;
				}
				break;
			}
		}
	}

	$last_tring = '';
	$index = '';
	while (count($tokens)) {
		$token = array_shift($tokens);
		switch ($token->id) {
			case T_CONSTANT_ENCAPSED_STRING:
				$last_tring = $token->text;
				break;
			case T_DOUBLE_ARROW:
				$index = trim($last_tring, "'\"");
				break;
			case T_WHITESPACE:
				// si c'est une nouvelle ligne, on est plus interesse par le commentaire
				if (str_contains($token->text, "\n") || str_contains($token->text, "\r")) {
					$index = '';
				}
				break;
			case T_COMMENT:
				if ($index && strpos($token->text, '#') === 0) {
					$comments[$index] = trim(substr($token->text, 1));
				}
				break;
		}
	}

	return $comments;
}


/**
 * Chargement des commentaires de fichier de langue SPIP < 5
 * Le fichier est chargé en mode texte pour récupérer les commentaires dans lesquels sont situés les statuts
 *
 * @param string $fichier_lang Le chemin du fichier de langue
 * @return array $liste_trad Un tableau id/chaine
 */
function salvatore_charger_commentaires_fichier_langue_spip($fichier_lang) {

	$contenu = file_get_contents($fichier_lang);
	$tokens = token_get_all($contenu);

	$comments = [];

	// allons jusqu'au debut du tableau
	while (count($tokens)) {
		$token = array_shift($tokens);
		if ($token[0] === T_ARRAY) {
			break;
		}
	}

	$last_tring = '';
	$index = '';
	while (count($tokens)) {
		$token = array_shift($tokens);
		switch ($token[0]) {
			case T_CONSTANT_ENCAPSED_STRING:
				$last_tring = $token[1];
				break;
			case T_DOUBLE_ARROW:
				$index = trim($last_tring, "'\"");
				break;
			case T_WHITESPACE:
				// si c'est une nouvelle ligne, on est plus interesse par le commentaire
				if (strpos($token[1], "\n") !== false || strpos($token[1], "\r") !== false) {
					$index = '';
				}
				break;
			case T_COMMENT:
				if ($index && strpos($token[1], '#') === 0) {
					$comments[$index] = trim(substr($token[1], 1));
				}
				break;
		}
	}
	return $comments;
}
