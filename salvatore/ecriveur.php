<?php

/*
	This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

	Salvatore is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	Trad-Lang is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Trad-Lang; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Copyright 2003-2020
		Florent Jugla <florent.jugla@eledo.com>,
		Philippe Riviere <fil@rezo.net>,
		Chryjs <chryjs!@!free!.!fr>,
		kent1 <kent1@arscenic.info>
		Cerdic <cedric@yterium.com>
*/

include_spip('base/abstract_sql');
include_spip('inc/charsets');
include_spip('inc/config');
include_spip('inc/filtres');
include_spip('inc/texte');
include_spip('inc/xml');
include_spip('inc/lang_liste');
include_spip('inc/session');


/**
 * @param array $liste_sources
 * @param string $message_commit
 * @param string $dir_modules
 * @param string $dir_depots
 * @throws Exception
 */
function salvatore_ecrire($liste_sources, $message_commit = '', $dir_modules = null, $dir_depots = null) {
	include_spip('inc/salvatore');
	salvatore_init();

	// on va lire dans la base, il faut qu'elle soit a jour
	salvatore_verifier_base_upgradee();

	if (is_null($dir_modules)) {
		$dir_modules = _DIR_SALVATORE_MODULES;
	}
	salvatore_check_dir($dir_modules);

	if (is_null($dir_depots)) {
		$dir_depots = _DIR_SALVATORE_DEPOTS;
	}
	salvatore_check_dir($dir_depots);

	$url_gestionnaire = salvatore_get_self_url();

	$nb_to_comit = 0;
	$modules_to_commit = [];
	foreach ($liste_sources as $source) {
		salvatore_log("\n<info>--- Module " . $source['module'] . ' | ' . $source['dir_module'] . ' | ' . $source['url'] . '</info>');

		$module = $source['module'];
		$dir_module = $dir_modules . $source['dir_module'];

		if ($autre_gestionnaire = salvatore_verifier_gestionnaire_traduction($dir_module, $module)) {
			salvatore_fail("[Ecriveur] Erreur sur $module", "Erreur : export impossible, le fichier est traduit autre part : $autre_gestionnaire\n");
		}

		$id_tradlang_module = sql_getfetsel('id_tradlang_module', 'spip_tradlang_modules', 'dir_module = ' . sql_quote($source['dir_module']));
		if (!$id_tradlang_module) {
			salvatore_fail("[Ecriveur] Erreur sur $module", "Erreur : export impossible, le module n'est pas en base\n");
		} else {
			// url de l'interface de traduction d'un module
			$url_trad_module = url_absolue(generer_objet_url($id_tradlang_module, 'tradlang_module'), $url_gestionnaire);
			$nb = salvatore_exporter_module($id_tradlang_module, $source, $url_gestionnaire, $url_trad_module, $dir_modules, $dir_depots, $message_commit);
			if ($nb > 0) {
				$modules_to_commit[] = $source['module'];
				$nb_to_comit += $nb;
			}
		}
	}

	if (!$nb_to_comit) {
		salvatore_log('<info>Rien a commit</info>');
	}
	else {
		salvatore_log("<info>$nb_to_comit commits a envoyer</info>");
		salvatore_log('Modules: ' . implode(', ', $modules_to_commit));
	}
}

/**
 * Genere les fichiers de traduction d'un module
 *
 * @param int $id_tradlang_module
 * @param array $source
 * @param string $url_site
 * @param string $url_trad_module
 * @param string $dir_modules
 * @param string $dir_depots
 * @param string $message_commit
 * @return false|int
 */
function salvatore_exporter_module($id_tradlang_module, $source, $url_site, $url_trad_module, $dir_modules, $dir_depots, $message_commit = '') {

	$url_repo = $source['url'];

	$row_module = sql_fetsel('*', 'spip_tradlang_modules', 'id_tradlang_module=' . (int) $id_tradlang_module);
	if (!$row_module) {
		$module = $source['module'];
		salvatore_log("<error>Le module #$id_tradlang_module $module n'existe pas</error>");
		return false;
	}

	$lang_ref = $row_module['lang_mere'];
	$dir_module = $dir_modules . $row_module['dir_module'];
	$module = $row_module['module'];
	$type_export = $row_module['type_export'];

	if (is_numeric($row_module['limite_trad']) && $row_module['limite_trad'] > 0) {
		$seuil_export = $row_module['limite_trad'];
	} else {
		$seuil_export = lire_config('tradlang/seuil_export_tradlang', _SALVATORE_SEUIL_EXPORT);
	}

	$file_commit_infos = $dir_module . '/' . $module . '.commit.json';
	if (file_exists($file_commit_infos)) {
		salvatore_fail("[Ecriveur] Erreur sur $module", "Erreur : il y a deja un fichier $file_commit_infos avec des commits en attente");
	}


	$xml_infos = $commit_infos = [];
	$liste_lang = $liste_lang_non_exportees = $liste_lang_a_supprimer = [];

	$count_trad_reference = sql_countsel('spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module . ' AND lang=' . sql_quote($row_module['lang_mere']) . " AND statut='OK'", 'id');
	$minimal = ceil((($count_trad_reference * $seuil_export) / 100));
	salvatore_log("Minimal = $minimal ($seuil_export %)");

	$langues = sql_allfetsel('lang,COUNT(*) as count', 'spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module . " AND statut != 'NEW' AND statut != 'attic'", 'lang', 'lang');
	foreach ($langues as $langue) {
		/**
		 * Le fichier est il suffisamment traduit
		 */
		if ($langue['count'] >= $minimal) {
			$liste_lang[] = $langue['lang'];
			$commit_infos[$langue['lang']] = [];
		}
		else {
			/**
			 * Le fichier n'est pas suffisamment traduit et n'existe pas, on ne fera donc rien
			 */
			if (!file_exists($dir_module . '/' . $module . '_' . $langue['lang'] . '.php')) {
				$liste_lang_non_exportees[] = $langue['lang'];
			} else {
				/**
				 * Il n'est pas suffisamment traduit, cependant, il existe déjà
				 * On ne va donc pas le supprimer à la barbare, mais on le met à jour quand même
				 */
				$liste_lang[] = $langue['lang'];
				$commit_infos[$langue['lang']] = [];
				$liste_lang_a_supprimer[] = $langue['lang'];
				$percent = (($langue['count'] / $count_trad_reference) * 100);
				if ($percent < ($seuil_export - 15)) {
					$commit_infos[$langue['lang']]['message'] = "La langue '" . $langue['lang'] . "' devrait être supprimée car trop peu traduite (" . number_format($percent, 2) . " %)\n";
				}
			}
		}
	}

	// traiter chaque langue
	foreach ($liste_lang as $lang) {
		salvatore_log("Generation de la langue $lang");
		$indent = "\t";

		$php_lines = $chaines = $id_tradlangs = [];
		$initiale = '';

		// On ne prend que les MODIF, les RELIRE et les OK pour ne pas rendre les sites multilingues en français
		$chaines = sql_allfetsel('id_tradlang,id,str,comm,statut,md5', 'spip_tradlangs', 'id_tradlang_module=' . (int) $id_tradlang_module . ' AND lang=' . sql_quote($lang) . " AND statut!='NEW' AND statut!='attic'", 'id');
		$id_tradlangs = array_column($chaines, 'id_tradlang');
		$chaines = array_combine(array_column($chaines, 'id'), $chaines);
		ksort($chaines);

		$total_chaines = ['OK' => 0, 'MODIF' => 0, 'RELIRE' => 0];
		foreach ($chaines as $chaine) {
			$total_chaines[$chaine['statut']]++;

			$comment = salvatore_clean_comment($chaine['comm']);

			if ($initiale !== strtoupper($chaine['id'][0])) {
				$initiale = strtoupper($chaine['id'][0]);
				$php_lines[] = "\n$indent// $initiale";
			}

			if (strlen($chaine['statut']) && $chaine['statut'] !== 'OK') {
				$comment .= ' ' . $chaine['statut'];
			}
			if ($comment) {
				$comment = ' # ' . trim($comment); // on rajoute les commentaires ?
			}

			// nettoyer la chaine de langue et calcul du md5
			$str = salvatore_nettoyer_chaine_langue($chaine['str'], $lang);
			$newmd5 = md5($str);

			/**
			 * Si le md5 ou la chaine à changé, on la met à jour dans la base
			 */
			if (($chaine['md5'] !== $newmd5) || ($str != $chaine['str'])) {
				$r = sql_updateq('spip_tradlangs', ['md5' => $newmd5, 'str' => $str], 'id_tradlang=' . (int) $chaine['id_tradlang']);
			}

			$php_lines[] = $indent . var_export($chaine['id'], 1) . ' => ' . var_export($str, 1) . ',' . $comment;
		}

		salvatore_log(' - traduction (' . $total_chaines['OK'] . "/$count_trad_reference OK | " . $total_chaines['RELIRE'] . "/$count_trad_reference RELIRE | " . $total_chaines['MODIF'] . "/$count_trad_reference MODIFS), export");

		$salvatore_exporter_fichier_php = 'salvatore_exporter_fichier_php_' . $type_export;
		if (!function_exists($salvatore_exporter_fichier_php)) {
			salvatore_log("<error>Pas de fonction pour l’export '$type_export'</error>");
			$salvatore_exporter_fichier_php = 'salvatore_exporter_fichier_php_spip5';
		}
		$file_name = $salvatore_exporter_fichier_php($dir_module, $module, $lang, $php_lines, $url_trad_module, ($lang == $lang_ref) ? $url_repo : false);

		// noter la langue et les traducteurs pour lang/module.xml
		$people_unique = [];
		$xml_infos[$lang] = [
			'traducteurs' => [],
			'traduits' => $total_chaines['OK'],
			'modifs' => $total_chaines['MODIF'],
			'relire' => $total_chaines['RELIRE']
		];
		if (defined('_ID_AUTEUR_SALVATORE') && (int) _ID_AUTEUR_SALVATORE > 0) {
			$people_unique[] = _ID_AUTEUR_SALVATORE;
		}

		// ici on prend tous les statut de chaine (?)
		$traducteurs = sql_allfetsel(
			'DISTINCT(traducteur)',
			'spip_tradlangs',
			[
				'id_tradlang_module=' . (int) $id_tradlang_module,
				'lang=' . sql_quote($lang),
				'traducteur IS NOT NULL',
				'traducteur != \'\''
			]
		);
		foreach ($traducteurs as $t) {
			$traducteurs_lang = explode(',', $t['traducteur']);
			foreach ($traducteurs_lang as $traducteur) {
				if (!in_array($traducteur, $people_unique)) {
					$traducteur_supp = [];
					if (is_numeric($traducteur) && ($id_auteur = (int) $traducteur)) {
						$traducteur_supp['nom'] = extraire_multi(sql_getfetsel('nom', 'spip_auteurs', 'id_auteur = ' . $id_auteur));
						$traducteur_supp['lien'] = url_absolue(generer_objet_url($id_auteur, 'auteur'), $url_site);
					} elseif (trim(strlen($traducteur)) > 0) {
						$traducteur_supp['nom'] = trim($traducteur);
						$traducteur_supp['lien'] = '';
					}
					if (isset($traducteur_supp['nom'])) {
						$xml_infos[$lang]['traducteurs'][strtolower($traducteur_supp['nom'])] = $traducteur_supp;
					}
					$people_unique[] = $traducteur;
				}
			}
		}
		unset($people_unique);

		if (salvatore_file_need_commit(basename($file_name), $source, $dir_depots)) {
			$commit_infos[$lang]['lang'] = $lang;
			$commit_infos[$lang]['file_name'] = basename($file_name);
			$commit_infos[$lang]['lastmodified'] = salvatore_read_lastmodified_file(basename($file_name), $source, $dir_depots);
			$commit_infos[$lang]['must_add'] = false;

			if ($row_module['limite_trad'] == 0) {
				$commit_infos[$lang]['must_add'] = true;
			} elseif (!in_array($module, ['ecrire', 'spip', 'public'])) {
				if (((int) (($xml_infos[$lang]['traduits'] / $count_trad_reference) * 100) > $seuil_export)) {
					$commit_infos[$lang]['must_add'] = true;
				}
			}

			// trouver le commiteur si c'est un fichier deja versionne ou a ajouter
			if ($commit_infos[$lang]['lastmodified'] || $commit_infos[$lang]['must_add']) {
				$where = [
					"objet='tradlang'",
					sql_in('id_objet', $id_tradlangs),
					"id_auteur != '-1'",
					'id_auteur !=' . (int) _ID_AUTEUR_SALVATORE,
				];
				if ($commit_infos[$lang]['lastmodified']) {
					$where[] = 'date>' . sql_quote(date('Y-m-d H:i:s', $commit_infos[$lang]['lastmodified']));
				}
				$auteur_versions = sql_allfetsel('DISTINCT id_auteur', 'spip_versions', $where);
				if ((is_countable($auteur_versions) ? count($auteur_versions) : 0) == 1) {
					$auteur = sql_fetsel('nom,email', 'spip_auteurs', 'id_auteur = ' . (int) $auteur_versions[0]['id_auteur']);
					if ($auteur && $auteur['email']) {
						$commit_infos[$lang]['author'] = $auteur['email'];
						if ($auteur['nom']) {
							$nom = $auteur['nom'];
							if (strpos($nom, '<') !== false) {
								$nom = extraire_multi($nom);
								$nom = textebrut($nom);
							}
							$commit_infos[$lang]['author'] = $nom . ' <' . $commit_infos[$lang]['author'] . '>';
						}
						salvatore_log("Le commiteur pour la langue $lang : " . $commit_infos[$lang]['author']);
					}
				}
			}
		} else {
			unset($commit_infos[$lang]);
		}
	}

	// le fichier XML recapitulatif
	$indent = "\t";
	$xml = "<traduction
{$indent}module=\"$module\"
{$indent}id=\"" . $row_module['dir_module'] . "\"
{$indent}gestionnaire=\"salvatore\"
{$indent}url=\"$url_site\"
{$indent}source=\"$url_repo\"
{$indent}reference=\"$lang_ref\">\n";
	foreach ($xml_infos as $lang => $info) {
		$detail = '';
		if ($info['traduits'] > 0) {
			$detail = " total=\"$count_trad_reference\" traduits=\"" . $info['traduits'] . '" relire="' . $info['relire'] . '" modifs="' . $info['modifs'] . '" nouveaux="' . ($count_trad_reference - ($info['modifs'] + $info['traduits'] + $info['relire'])) . '" pourcent="' . number_format((($info['traduits'] / $count_trad_reference) * 100), 2) . '"';
		}
		if ($info['traducteurs'] !== []) {
			$xml .= "$indent<langue code=\"$lang\" url=\"" . parametre_url($url_trad_module, 'lang_cible', $lang) . "\"{$detail}>\n";
			ksort($info['traducteurs']);
			foreach ($info['traducteurs'] as $nom => $people) {
				$xml .= $indent . $indent . '<traducteur nom="' . entites_html($people['nom']) . '" lien="' . entites_html($people['lien']) . "\" />\n";
			}
			$xml .= "$indent</langue>\n";
		} else {
			$xml .= "$indent<langue code=\"$lang\" url=\"" . parametre_url($url_trad_module, 'lang_cible', $lang) . "\"{$detail} />\n";
		}
	}
	$xml .= "</traduction>\n";
	$file_xml = $dir_module . '/' . $module . '.xml';
	file_put_contents($file_xml, $xml);
	if (salvatore_file_need_commit(basename($file_xml), $source, $dir_depots)) {
		$commit_infos['.xml'] = [
			'file_name' => basename($file_xml),
			'lastmodified' => salvatore_read_lastmodified_file(basename($file_xml), $source, $dir_depots),
			'must_add' => true
		];
	}


	if (isset($liste_lang_non_exportees) && $liste_lang_non_exportees !== []) {
		salvatore_log('Les langues suivantes ne sont pas exportées car trop peu traduites : ' . implode(', ', $liste_lang_non_exportees));
	}
	if (isset($liste_lang_a_supprimer) && $liste_lang_a_supprimer !== []) {
		salvatore_log('<error>Les langues suivantes devraient être supprimées car trop peu traduites : ' . implode(', ', $liste_lang_a_supprimer) . '</error>');
	}

	$nb_to_commit = 0;
	// et on ecrit un json pour que le pousseur sache quoi commit
	if ($commit_infos !== []) {
		$nb_to_commit = count($commit_infos);
		if ($message_commit) {
			$commit_infos['.message'] = $message_commit;
		}
		file_put_contents($file_commit_infos, json_encode($commit_infos, JSON_THROW_ON_ERROR));
	}

	if ($row_module['bon_a_pousser'] > 0) {
		sql_updateq('spip_tradlang_modules', ['bon_a_pousser' => 0], 'id_tradlang_module=' . (int) $id_tradlang_module);
	}

	$log = salvatore_read_status_modif($module, $source, $dir_depots);
	salvatore_log($log);
	return $nb_to_commit;
}

/**
 * Nettoyer le commentaire avant ecriture dans le PHP
 * @param $comment
 * @return mixed|string
 */
function salvatore_clean_comment($comment) {
	if (strlen(trim($comment)) > 1) {
		// On remplace les sauts de lignes des commentaires sinon ça crée des erreurs php
		$comment = str_replace(["\r\n", "\n", "\r"], ' ', $comment);
		// Conversion des commentaires en utf-8
		$comment = unicode_to_utf_8(html_entity_decode(preg_replace('/&([lg]t;)/S', '&amp;\1', $comment), ENT_NOQUOTES, 'utf-8'));
		return $comment;
	}
	return '';
}


/**
 * Generer un fichier de langue (SPIP < 5) a partir de ses lignes php
 *
 * À partir de SPIP 4.1, les fichiers de langues SPIP peuvent retourner
 * directement un array (plus propre que de peupler une globale).
 * Cette méthode d’écriture sera donc à déprécier / supprimer dans quelques années
 * pour utiliser uniquement `spip5`
 *
 * @param string $dir_module
 * @param string $module
 * @param string $lang
 * @param array $php_lines
 * @param string $url_trad_module
 * @param $origin
 * @return string
 */
function salvatore_exporter_fichier_php_spip($dir_module, $module, $lang, $php_lines, $url_trad_module, $origin) {
	$file_name = $dir_module . '/' . $module . '_' . $lang . '.php';
	$file_content = '<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
';
	if ($origin) {
		$file_content .= '// Fichier source, a modifier dans ' . $origin . "\n";
	} else {
		$url_trad_module = parametre_url($url_trad_module, 'lang_cible', $lang, '&');
		$file_content .= '// extrait automatiquement de ' . $url_trad_module . '
// ** ne pas modifier le fichier **' . "\n\n";
	}

	// historiquement les fichiers de lang de spip_loader ne peuvent pas etre securises
	if ($module !== 'tradloader') {
		$file_content .= "if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}\n\n";
	} elseif ($origin) {
		$file_content .= "\n";
	}

	# supprimer la virgule du dernier item
	$php_lines[count($php_lines) - 1] = preg_replace('/,([^,]*)$/', '\1', $php_lines[count($php_lines) - 1]);

	$file_content .=
		'$GLOBALS[$GLOBALS[\'idx_lang\']] = array(' . "\n"
		. implode("\n", $php_lines)
	  . "\n);\n";
	file_put_contents($file_name, $file_content);
	// et on fixe le chmod au passage
	chmod($file_name, 0644);
	return $file_name;
}


/**
 * Generer un fichier de langue (SPIP >= 4.1) a partir de ses lignes php
 * @param string $dir_module
 * @param string $module
 * @param string $lang
 * @param array $php_lines
 * @param string $url_trad_module
 * @param $origin
 * @return string
 */
function salvatore_exporter_fichier_php_spip5($dir_module, $module, $lang, $php_lines, $url_trad_module, $origin) {
	$file_name = $dir_module . '/' . $module . '_' . $lang . '.php';
	$file_content = '<?php' . "\n";
	$file_content .= '// This is a SPIP language file  --  Ceci est un fichier langue de SPIP' . "\n";

	if ($origin) {
		$file_content .= '// Fichier source, a modifier dans ' . $origin . "\n\n";
	} else {
		$url_trad_module = parametre_url($url_trad_module, 'lang_cible', $lang, '&');
		$file_content .= '// extrait automatiquement de ' . $url_trad_module . "\n";
		$file_content .= '// ** ne pas modifier le fichier **' . "\n\n";
	}

	$file_content .= 'return [' . "\n" . implode("\n", $php_lines) . "\n];\n";
	file_put_contents($file_name, $file_content);
	// et on fixe le chmod au passage
	chmod($file_name, 0644);
	return $file_name;
}


/**
 * Lire la date de derniere modif d'un fichier de langue
 * @param string $file_name
 * @param array $source
 * @param string $dir_depots
 * @return false|int
 */
function salvatore_read_lastmodified_file($file_name, $source, $dir_depots) {

	$file_path_relative = $file_name;
	if ($source['dir']) {
		$file_path_relative = $source['dir'] . DIRECTORY_SEPARATOR . $file_path_relative;
	}

	$vcs_lastmodified_file = salvatore_vcs_function($source['methode'], 'lastmodified_file');
	return $vcs_lastmodified_file($dir_depots . $source['dir_checkout'], $file_path_relative);
}


/**
 * Afficher le status des fichiers modifies pour un module
 * @param string $module
 * @param array $source
 * @param $dir_depots
 * @return string
 */
function salvatore_read_status_modif($module, $source, $dir_depots) {
	$pre = '';
	if ($source['dir']) {
		$pre = $source['dir'] . DIRECTORY_SEPARATOR;
	}
	$files_list = [$pre . $module . '_*', $pre . $module . '.xml'];

	$vcs_status_file = salvatore_vcs_function($source['methode'], 'status_file');
	return $vcs_status_file($dir_depots . $source['dir_checkout'], $files_list);
}


/**
 * Recuperer le status d'un fichier pour voir si vide ou si modifie/nouveau
 * @param string $file
 * @param array $source
 * @param $dir_depots
 * @return string
 */
function salvatore_file_need_commit($file, $source, $dir_depots) {
	$pre = '';
	if ($source['dir']) {
		$pre = $source['dir'] . DIRECTORY_SEPARATOR;
	}

	$vcs_status_file = salvatore_vcs_function($source['methode'], 'status_file');
	$status = $vcs_status_file($dir_depots . $source['dir_checkout'], $pre . $file);
	return ((bool) strlen(trim($status)));
}
