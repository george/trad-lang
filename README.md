# Plugin trad-lang

Ce plugin gère une interface d’édition des fichiers de langue pour les traductrices & traducteurs
qui sert au site trad.spip.net notamment.

Il lit et écrit directement dans les dépots Git correspondants en tant qu’utilisateur `Salvatore`
