<?php

/**
 *
 * Trad-lang v2
 * Plugin SPIP de traduction de fichiers de langue
 * © Florent Jugla, Fil, kent1
 *
 * Action permettant de récupérer un fichier de langue
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_tradlang_exporter_langue_dist() {
	$level = null;
	$lang_crea = null;
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	if (!preg_match(',^(\d+)\/(\w+)?(\/?(\w*))?(\/?(\w*))$,', $arg, $r)) {
		spip_log("action_tradlang_exporter_langue_dist $arg pas compris", 'tradlang');
	}

	$id_tradlang_module = (int) $r[1];
	$lang_cible = $r[2];
	$type = $r[4] ?: false;
	$tout = $r[6] != 'non';
	if ($lang_cible && (int) $id_tradlang_module && sql_countsel('spip_tradlangs', 'id_tradlang_module = ' . (int) $id_tradlang_module . ' AND lang = ' . sql_quote($lang_cible))) {
		$module = sql_getfetsel('module', 'spip_tradlang_modules', 'id_tradlang_module = ' . (int) $id_tradlang_module);
		$tradlang_sauvegarde_module = charger_fonction('tradlang_sauvegarde_module', 'inc');
		$fichier = $tradlang_sauvegarde_module($module, $lang_cible, false, $type, $tout);
		if (file_exists($fichier)) {
			switch ($type) {
				case 'po':
					$content_type = 'application/x-gettext';
					break;
				case 'csv':
					$content_type = 'text/csv';
					break;
				case 'php':
					$content_type = 'application/x-httpd-php';
					break;
				default:
					$content_type = 'application/octet-stream';
					break;
			}
			include_spip('inc/livrer_fichier');
			spip_livrer_fichier($fichier, $content_type);
			die();
		} else {
			include_spip('inc/minipres');
			echo minipres();
		}
	} else {
		include_spip('inc/minipres');
		echo minipres();
	}
	$redirect = _request('redirect');
	if ($redirect) {
		$redirect = parametre_url($redirect, 'var_lang_crea', $lang_crea, '&');
		include_spip('inc/headers');
		redirige_par_entete($redirect);
	}
}
