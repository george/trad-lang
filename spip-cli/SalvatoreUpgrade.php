<?php

/*
    This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

    Salvatore is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2018
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>,
        Chryjs <chryjs!@!free!.!fr>,
 		kent1 <kent1@arscenic.info>
*/

/**
 * Ce script va chercher les fichiers définis dans le fichier traductions/traductions.txt
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class SalvatoreUpgrade extends Command {
	protected function configure(){
		$this
			->setName('salvatore:upgrade')
			->setDescription('Mets a jour la base de donnees et migre depuis une ancienne version')
			->addOption(
				'traductions',
				null,
				InputOption::VALUE_REQUIRED,
				"Chemin vers le fichier traductions.txt a utiliser [salvatore/traductions/traductions.txt] pour mettre a jour les modules en base lors de l'upgrade\nIndiquez la valeur 'force' pour forcer la fin de la mise a jour meme si des modules restent non upgrades",
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){

		include_spip('inc/salvatore');
		include_spip('salvatore/lecteur');

		salvatore_init([$output, 'writeln']);


		$output->writeln("<comment>=======================================</comment>");
		$output->writeln("<comment>UPGRADE [Mise à jour de la base]</comment>");
		$output->writeln("<comment>=======================================</comment>");

		define('_TIME_OUT', time() + 24*3600); // pas de timeout
		include_spip('tradlang_administrations');

		// commencer par la
		include_spip('inc/filtres');
		$schema_declare = filtre_info_plugin_dist('tradlang', 'schema');
		tradlang_upgrade('tradlang_base_version', $schema_declare);
		$output->writeln("-");

		$modules_todo = sql_allfetsel('distinct module','spip_tradlang_modules', "dir_module='' OR dir_module=module");
		$modules_todo = array_column($modules_todo, 'module');
		$n = count($modules_todo);
		$output->writeln("$n modules en base sans dir_module");

		$traductions = $input->getOption('traductions');
		if ($traductions === 'force') {
			if ($n > 0) {
				$output->writeln("<info>Forcer un dir_module arbitraire pour les modules restant :</info>");
				$output->writeln(implode(', ', $modules_todo));
				sql_update('spip_tradlang_modules', ['dir_module' => "CONCAT('--',module)"], "dir_module='' OR dir_module=module");
			}
		}
		elseif ($traductions) {
			$liste_trad = salvatore_charger_fichier_traductions($traductions);
			$n = count($liste_trad);
			$output->writeln("<info>$n modules dans le fichier traductions " . ($traductions ?: '') . "</info>");

			foreach ($liste_trad as $traduction) {
				if (in_array($traduction['module'], $modules_todo)) {
					sql_updateq('spip_tradlang_modules', ['dir_module' => $traduction['dir_module']], "module=".sql_quote($traduction['module'])." AND (dir_module='' OR dir_module=module)");
					$output->writeln("  Module " . $traduction['module'] . " -> " . $traduction['dir_module']);
				}
			}
		}


		$modules_todo = sql_allfetsel('distinct module','spip_tradlang_modules', "dir_module='' OR dir_module=module");
		$modules_todo = array_column($modules_todo, 'module');
		if ($n = count($modules_todo)) {
			$this->io->error("Encore $n modules en base sans dir_module : \n" . implode(', ', $modules_todo)."\n\nEssayez avec un fichier traductions complementaire");
			return self::FAILURE;
		}


		// et ca doit finir nicely
		try {
			salvatore_verifier_base_upgradee();
		}
		catch (Exception $e) {
			$this->io->error("Upgrade incomplet : \n" . $e->getMessage()."\n\nEssayez avec un fichier traductions complementaire");
			return self::FAILURE;
		}
		$output->writeln("<info>Upgrade Complet</info>");

		return self::SUCCESS;
	}
}

