<?php

/*
    This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

    Salvatore is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2020
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>,
        Chryjs <chryjs!@!free!.!fr>,
        kent1 <kent1@arscenic.info>
        Cerdic <cedric@yterium.com>
*/

/**
 * Prend les fichiers de langue de référence de salvatore/modules/ et met à jour la base de données
 * pour les modules decrits dans le fichier traductions/traductions.txt
 *
 */


use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class SalvatoreRecharger extends Command {
	protected function configure(){
		$this
			->setName('salvatore:recharger')
			->setDescription('Tirer&Lire les modules modifies des fichiers de langue de référence de salvatore/modules/')
			->addOption(
				'traductions',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le fichier traductions.txt a utiliser [salvatore/traductions/traductions.txt]',
				null
			)
			->addOption(
				'changedir',
				null,
				InputOption::VALUE_REQUIRED,
				'Chemin vers le repertoires qui contient les traces JSON posees par le debardeur',
				null
			)
			->addOption(
				'from',
				null,
				InputOption::VALUE_REQUIRED,
				'Date ou intervalle de temps sur lequel considerer les changements. Defaut : -1hour',
				null
			)
			->addOption(
				'module',
				null,
				InputOption::VALUE_REQUIRED,
				'Un ou plusieurs modules a traiter (par defaut tous les modules du fichier de traduction seront traites)',
				null
			)
			->addOption(
				'force',
				null,
				InputOption::VALUE_NONE,
				'Forcer la relecture du ou des modules et la mise a jour en base indépendament de la date de dernière mise a jour des fichiers',
				null
			)
			->addOption(
				'time',
				null,
				InputOption::VALUE_NONE,
				'Ajouter date/heure sur les sorties pour les logs',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output){
		include_spip('inc/salvatore');

		$time = $input->getOption('time');
		salvatore_init([$output, 'writeln'], (bool) $time);

		salvatore_log("<comment>=======================================</comment>");
		salvatore_log("<comment>RECHARGER [Tirer&Lire les modules modifies sur la zone]</comment>");
		salvatore_log("<comment>=======================================</comment>");


		$traductions = $input->getOption('traductions');
		$liste_trad = salvatore_charger_fichier_traductions($traductions);
		$n = count($liste_trad);
		salvatore_log("<info>$n modules dans le fichier traductions " . ($traductions ?: '') . "</info>");

		$modules = $input->getOption('module');
		if ($modules = trim($modules)) {
			$liste_trad = salvatore_filtrer_liste_traductions($liste_trad, $modules);
			$n = count($liste_trad);
			salvatore_log("<info>$n modules à traiter : " . $modules . "</info>");
		}

		// https://git.spip.net/spip-contrib-extensions/debardeur/src/branch/master/action/api_debardeur_hook.php#L224
		$changedir = $input->getOption('changedir');
		if (empty($changedir) || !is_dir($changedir)) {
			salvatore_log("<error>Indiquez un repertoire valide pour changedir</error>");
			return self::FAILURE;
		}

		$changed = $this->lister_depots_modifies($changedir);
		if (empty($changed)) {
			salvatore_log("<info>Rien a faire, changedir vide</info>");
			return self::SUCCESS;
		}

		$from = $input->getOption('from');
		if (!$from) {
			$from = '-1hour';
		}
		$t_since = strtotime($from);
		if (!$t_since) {
			salvatore_log("<error>Indiquez un temps valide pour l'option --from</error>");
			return self::FAILURE;
		}

		$changed_trad = $this->filter_changed_traductions($liste_trad, $changed, $t_since);
		if ($changed_trad) {
			$n = is_countable($changed_trad) ? count($changed_trad) : 0;
			$changed_modules = array_column($changed_trad, 'module');
			$changed_modules = array_unique($changed_modules);
			salvatore_log("<info>$n modules modifiés à recharger : " . implode(',', $changed_modules) . "</info>");

			include_spip('salvatore/lecteur');
			include_spip('salvatore/tireur');
			salvatore_tirer($changed_trad);

			$force = $input->getOption('force');
			salvatore_lire($changed_trad, $force);
		}
		else {
			salvatore_log("<info>Rien a faire, aucun fichier correspondant a un module</info>");
		}

		return self::SUCCESS;

	}

	protected function lister_depots_modifies($changedir) {

		$changed = [];
		$files = glob(rtrim($changedir,'/')."/*.json");
		foreach ($files as $file) {
			if (
				($json = file_get_contents($file))
				&& ($json = json_decode($json, true, 512, JSON_THROW_ON_ERROR))
			) {
				if (empty($json['time'])) {
					$json['time'] = filemtime($file);
				}
				$changed[] = $json;
			}
			@unlink($file);
		}

		return $changed;
	}



	protected function filter_changed_traductions($liste_trad, $changed, $t_since) {

		$changed_trad = [];
		salvatore_log((is_countable($changed) ? count($changed) : 0) . " depots changés");

		foreach($changed as $c) {
			/*
			[
				'methode' => 'git',
				'url' => $url,
				'branche' => $branch,
				'short' => $data['repository']['name'],
				'time' => time(),
			];
			 */

			if ($c['time'] >= $t_since) {
				foreach ($liste_trad as $k=>$source) {
					if (
						$source['url'] === $c['url']
						&& $source['branche'] === $c['branche']
					) {
						$changed_trad[$k] = $source;
						unset($liste_trad[$k]);
					}
				}
			}
		}
		// on remet dans le meme ordre que la liste d'origine
		ksort($changed_trad);

		return $changed_trad;
	}
}

