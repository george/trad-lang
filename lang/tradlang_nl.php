<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradlang?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_distribution' => 'Geen enkele distributie beschikbaar.',
	'aucunmodule' => 'Geen enkele module.',
	'auteur_revision' => '@nb@ aanpassing van de vertaling.',
	'auteur_revision_specifique' => '@nb@ aanpassing van de vertaling in het <abbr title="@lang@">@langue_longue@</abbr>.',
	'auteur_revisions' => '@nb@ aanpassingen van de vertaling.',
	'auteur_revisions_langue' => 'Zijn ingebrachte taal:',
	'auteur_revisions_langues' => 'De @nb@ ingebrachte talen:',
	'auteur_revisions_specifique' => '@nb@ aanpassingen van de vertalingen in het <abbr title="@lang@">@langue_longue@</abbr>.',

	// B
	'bouton_activer_lang' => 'De taal "@lang@" activeren voor deze module',
	'bouton_exporter_fichier' => 'Exporteer het bestand',
	'bouton_exporter_fichier_langue' => 'Exporteer het taalbestand in het "@lang@"',
	'bouton_exporter_fichier_langue_complet' => 'Exporteer het volledige taalbestand in het "@lang@"',
	'bouton_exporter_fichier_langue_original' => 'Exporteer het oorspronkelijke taalbestand ("@lang_mere@")',
	'bouton_exporter_fichier_langue_po' => 'Exporteer het taalbestand in het "@lang@" in formaat .po',
	'bouton_exporter_fichier_po' => 'Exporteer het bestand in .po',
	'bouton_exporter_fichier_zip' => 'Exporteer de bestanden in zip',
	'bouton_precedent' => 'Vorige stap',
	'bouton_suivant' => 'Volgende stap',
	'bouton_supprimer_langue_module' => 'Deze taal van de module verwijderen',
	'bouton_supprimer_module' => 'Verwijder deze module',
	'bouton_traduire' => 'Vertalen',
	'bouton_upload_langue_module' => 'Verstuur een taalbestand',
	'bouton_vos_favoris_non' => 'Je niet-favoriete modules',
	'bouton_vos_favoris_oui' => 'Je favoriete modules',
	'bouton_vos_favoris_tous' => 'Alle modules',

	// C
	'cfg_form_tradlang_autorisations' => 'De autorisaties',
	'cfg_inf_type_autorisation' => 'Wanneer je per status of per auteur kiest, wordt hieronder je selectie gevraagd.',
	'cfg_lbl_autorisation_auteurs' => 'Toestaan per lijst van auteurs',
	'cfg_lbl_autorisation_statuts' => 'Toestaan per status van auteurs',
	'cfg_lbl_autorisation_webmestre' => 'Alleen webmasters toestaan',
	'cfg_lbl_liste_auteurs' => 'Auteurs van de site',
	'cfg_lbl_statuts_auteurs' => 'Mogelijke statussen',
	'cfg_lbl_type_autorisation' => 'Autorisatiemethode',
	'cfg_legend_autorisation_configurer' => 'De plugin beheren',
	'cfg_legende_autorisation_modifier' => 'De vertalingen aanpassen',
	'cfg_legende_autorisation_voir' => 'De vertaalinterface zien',
	'codelangue' => 'Taalcode',
	'confirm_suppression_langue_cible' => 'Let op, je gaat taal « <b>@lang@</b> » van deze module verwijderen. Weet je het zeker?',
	'crayon_changer_statut' => 'Let op! Je hebt de inhoud van de string aangepast zonder de status te wijzigen.',
	'crayon_changer_statuts' => 'Let op! Je hebt de inhoud van een of meer strings aangepast zonder de status te wijzigen.',

	// E
	'entrerlangue' => 'Een taalcode toevoegen',
	'erreur_aucun_item_langue_mere' => 'De moedertaal "@lang_mere@" bevat geen enkel taalitem.',
	'erreur_aucun_module' => 'Er zijn geen modules in de database beschikbaar.',
	'erreur_aucun_tradlang_a_editer' => 'Geen enkele taalstring wordt onvertaald beschouwd.',
	'erreur_autorisation_modifier_modules' => 'Je mag de taalmodules niet aanpassen.',
	'erreur_autoriser_profil' => 'Je mag dit profiel niet aanpassen.',
	'erreur_choisir_lang_cible' => 'Kies een doeltaal voor de vertaling.',
	'erreur_choisir_lang_orig' => 'Kies een brontaal die als basis voor de vertaling dient.',
	'erreur_choisir_module' => 'Kie een te vertalen module.',
	'erreur_code_langue_existant' => 'Deze taalvariant bestaat al voor deze module',
	'erreur_code_langue_invalide' => 'Deze taalcode is ongeldig',
	'erreur_langue_activer_impossible' => 'De taalcode "@lang@" bestaat niet.',
	'erreur_langues_autorisees_insuffisantes' => 'Je moet tenminste twee talen kiezen',
	'erreur_langues_differentes' => 'Kies een andere doeltaal dan de brontaal',
	'erreur_limite_trad_invalide' => 'Deze waarde moet een getal zijn tussen 0 en 100',
	'erreur_modif_tradlang_session' => 'Je kunt dit taalitem niet aanpassen.',
	'erreur_modif_tradlang_session_identifier' => 'Identificeer jezelf.',
	'erreur_module_inconnu' => 'Deze module is niet beschikbaar',
	'erreur_pas_langue_cible' => 'Kies een doeltaal',
	'erreur_repertoire_local_inexistant' => 'Let op: de map om lokaal op te slaan "squelettes/lang" bestaat niet',
	'erreur_statut_js' => 'De taalstring werd aangepast, maar niet zijn status',
	'erreur_upload_aucune_modif' => 'In je bestand zit geen enkele aanpassing',
	'erreur_upload_choisir_une' => 'Je moet tenminste één aanpassing valideren',
	'erreur_upload_fichier_php' => 'Je bestand "@fichier@" komt niet overeen met het verwachte bestand "@fichier_attendu@".',
	'erreur_variable_manquante' => 'Het deel van de volgende string moet niet worden vertaald:',
	'erreur_variable_manquante_js' => 'Een of meerdere verplichte variabelen werden aangepast',
	'erreur_variable_manquantes' => 'De @nb@ delen van de volgende string moeten niet worden aangepast:',
	'explication_comm' => 'Het commentaar is een in het taalbestand toegevoegde tekst bedoeld om bijvoorbeeld een bepaalde vertaalkeuze voor te stellen.',
	'explication_export_fichier_complet' => 'Het volgende bestand is onvolledig. Alle al dan niet vertaalde stings zijn beschikbaar.<br />Het kan als basis voor de vertaling worden gebruikt.',
	'explication_export_fichier_original' => 'Het volgende bestand is het taalbestand in de oorspronkelijke taal.<br />Het kan als basis voor de vertaling worden gebruikt.',
	'explication_export_fichier_po' => 'Het volgende bestand is in "po" formaat. Alle al dan niet vertaalde taalstrings zijn beschikbaar.<br />Het moet worden gebruikt met vertaalsoftware die dit formaat accepteert en kan vervolgens worden geïmporteerd met de knop voor het verzenden van een taalbestand.',
	'explication_langue_cible' => 'De taal waarin je vertaalt.',
	'explication_langue_origine' => 'De taal van waaruit je vertaalt (Uitsluitend 100% volledige talen zijn beschikbaar).',
	'explication_langues_autorisees' => 'Gebruikers kunnen uitsluitend in de geselecteerde talen vertalen.',
	'explication_limiter_langues_bilan' => 'Standaard worden @nb@ talen getoond, wanneer de gebruikers geen voorkeurtalen in hun profiel hebben opgenomen.',
	'explication_limiter_langues_bilan_nb' => 'Hoeveel talen worden standaard getoond (de meest vertaalde worden geselecteerd).',
	'explication_sauvegarde_locale' => 'Zal de bestanden opslaan in de skelettenmap van de site',
	'explication_sauvegarde_post_edition' => 'Zal tijdelijke bestanden opslaan bij iedere aanpassing van een taalstring',

	// F
	'favoris_ses_modules' => 'Zijn favoriete modules',
	'favoris_vos_modules' => 'Jouw favoriete modules',

	// I
	'icone_modifier_tradlang' => 'Deze taalstring aanpassen',
	'icone_modifier_tradlang_module' => 'Deze taalmodule aanpassen',
	'importer_module' => 'Een nieuwe taalmodule importeren',
	'importermodule' => 'Een module importeren',
	'info_1_tradlang' => '@nb@ taalstring',
	'info_1_tradlang_module' => '1 taalmodule',
	'info_aucun_participant_lang' => 'Nog geen enkele auteur van de site heeft in het <abbr title="@lang@">@langue_longue@</abbr> vertaald.',
	'info_aucun_tradlang_module' => 'Geen enkele taalmodule',
	'info_auteur_sans_favori' => 'Deze auteur heeft geen enkele favoriete module.',
	'info_chaine_jamais_modifiee' => 'Deze string werd nog nooit aangepast.',
	'info_chaine_originale' => 'Dit is de originele string',
	'info_choisir_langue' => 'In een specifieke taal',
	'info_contributeurs' => 'Medewerkers',
	'info_distributions' => 'De distributies',
	'info_edition_par_lot' => 'Uitgave per lot:',
	'info_export' => 'Export:',
	'info_filtrer_status' => 'Filteren op status:',
	'info_langue_mere' => '(moedertaal)',
	'info_langues_non_preferees' => 'Andere talen:',
	'info_langues_preferees' => 'Voorkeurta(a)l(en):',
	'info_module_inexistant_lang' => 'Deze module bestaat nog niet in taal "@lang@"',
	'info_module_nb_items_langue_mere' => 'De moedertaal van de module is <abbr title="@lang_mere@">@lang_mere_longue@</abbr> en bevat @nb@ taalitems.',
	'info_module_traduction' => '@statut@: @total@ (@percent@%)',
	'info_module_traduit_langues' => 'Deze module is (gedeeltelijk) vertaald in @nb@ talen.',
	'info_module_traduit_pc' => 'Module vertaald op @pc@%',
	'info_module_traduit_pc_lang' => 'Module "@module@" vertaald op @pc@% in het @lang@ (@langue_longue@)',
	'info_modules_priorite_traduits_pc' => 'De modules met prioriteit "@priorite@" zijn op @pc@% vertaald in het @lang@',
	'info_nb_items_module' => '@nb@ items in de module "@module@"',
	'info_nb_items_module_modif' => '@nb@ items van de module "@module@" zijn aangepast en te controleren in het @lang@ (@langue_longue@)"',
	'info_nb_items_module_modif_aucun' => 'Geen enkel item van module "@module@" is aangepast en te controleren in het @lang@ (@langue_longue@)',
	'info_nb_items_module_modif_un' => 'Een item van module "@module@" is aangepast en te controleren in het @lang@ (@langue_longue@)',
	'info_nb_items_module_new' => '@nb@ items van module "@module@" moeten in het @lang@ (@langue_longue@) vertaald worden',
	'info_nb_items_module_new_aucun' => 'Geen enkel item van module "@module@" moet vertaald worden in het @lang@ (@langue_longue@)',
	'info_nb_items_module_new_un' => 'Een item van module "@module@" moet vertaald worden in het @lang@ (@langue_longue@)"',
	'info_nb_items_module_ok' => '@nb@ items van module "@module@" zijn vertaald in het @lang@ (@langue_longue@)"',
	'info_nb_items_module_ok_aucun' => 'Geen enkel item van module "@module@" is vertaald in het @lang@ (@langue_longue@)',
	'info_nb_items_module_ok_un' => 'Een item van module "@module@" is vertaald in het @lang@ (@langue_longue@)"',
	'info_nb_items_module_relire' => '@nb@ items van module "@module@" moeten opnieuw bekeken worden in het @lang@ (@langue_longue@)"',
	'info_nb_items_module_relire_aucun' => 'Geen enkel item van module "@module@" moet opnieuw bekeken worden in het @lang@ (@langue_longue@)',
	'info_nb_items_module_relire_un' => 'Een item van module "@module@" moet opnieuw bekeken worden in het @lang@ (@langue_longue@)"',
	'info_nb_items_priorite' => 'De modules met prioriteit "@priorite@" bevatten @nb@ items',
	'info_nb_items_priorite_modif' => '@pc@% van de items de met prioriteit "@priorite@" sont modifiés et à vérifier in het @lang@ (@langue_longue@)',
	'info_nb_items_priorite_new' => '@pc@% van de items met prioriteit "@priorite@" zijn nieuw in het @lang@ (@langue_longue@)',
	'info_nb_items_priorite_ok' => 'De modules met prioriteit "@priorite@" zijn vertaald op @pc@% in het @lang@ (@langue_longue@)',
	'info_nb_items_priorite_relire' => '@pc@% van de items met prioriteit "@priorite@" moeten opnieuw gelezen worden in het @lang@ (@langue_longue@)',
	'info_nb_modules_favoris' => '@nb@ favoriete modules.',
	'info_nb_participant' => '@nb@ auteur ingeschreven op deze site heeft ten minste éénmaal aan een vertaling meegewerkt.',
	'info_nb_participant_lang' => '@nb@ auteur ingeschreven op deze site heeft ten minste éénmaal aan de vertaling in het <abbr title="@lang@">@langue_longue@</abbr> meegewerkt.',
	'info_nb_participants' => '@nb@ auteurs ingeschreven op deze site heeft ten minste éénmaal aan een vertaling meegewerkt.',
	'info_nb_participants_lang' => '@nb@ auteurs ingeschreven op deze site heeft ten minste éénmaal aan een vertaling in het <abbr title="@lang@">@langue_longue@</abbr> meegewerkt.',
	'info_nb_tradlang' => '@nb@ taalstrings',
	'info_nb_tradlang_module' => '@nb@ taalmodules',
	'info_percent_chaines' => '@traduites@ / @total@ strings vertaald in het "[@langue@] @langue_longue@"',
	'info_revisions_stats' => 'Revisies',
	'info_status_ok' => 'OK',
	'info_statut' => 'Status',
	'info_str' => 'Tekst van de taalstring',
	'info_textarea_readonly' => 'Dit tekstveld kan uitsluitend worden gelezen',
	'info_tradlangs_sans_version' => '@nb@ taalstrings hebben nog geen eerstgemaakte revisie (eerste revisies worden door CRON gemaakt).',
	'info_traducteur' => 'Vertaler(s)',
	'info_traduire_module_lang' => 'Module "@module@" vertalen in het @langue_longue@ (@lang@)',
	'infos_trad_module' => 'Informatie over vertalingen',
	'item_creer_langue_cible' => 'Maak een nieuwe doeltaal',
	'item_groupes_association_modules' => 'De taalmodules',
	'item_langue_cible' => 'De doeltaal: ',
	'item_langue_origine' => 'De oorspronkelijke taal:',
	'item_manquant' => '1 item ontbreekt in deze taal (ten opzichte van de moedertaal)',
	'item_non_defini_fichier' => '@nb@ taalstring is niet in het aangeleverde bestand opgenomen.',
	'item_non_defini_fichier_nb' => '@nb@ taalstrings zijn niet in het aangeleverde bestand opgenomen',
	'items_en_trop' => '@nb@ items te veel in deze taal (ten opzichte van de moedertaal)',
	'items_manquants' => '@nb@ items ontbreken in deze taal (ten opzichte van de moedertaal)',
	'items_modif' => 'Aangepaste items:',
	'items_new' => 'Nieuwe items:',
	'items_relire' => 'Onieuw te lezen items:',
	'items_total_nb' => 'Totaal aantal items:',

	// J
	'job_creation_revisions_modules' => 'Aanmaken van de bronrevisies van module "@module@"',

	// L
	'label_descriptif_priorite' => 'Prioriteit "@priorite@"',
	'label_fichier_langue' => 'Online te zetten taalbestand',
	'label_id_tradlang' => 'Identificatie van de string',
	'label_idmodule' => 'ID van de module',
	'label_lang' => 'Taal',
	'label_langue_mere' => 'Moedertaal',
	'label_langues_autorisees' => 'Slechts bepaalde talen toestaan',
	'label_langues_preferees_auteur' => 'Je voorkeurta(a)l(en)',
	'label_langues_preferees_autre' => 'Zijn voorkeurta(a)l(en)',
	'label_limite_trad' => 'Vertaaldrempel om te exporteren',
	'label_limiter_langues_bilan' => 'Het aantal zichtbare talen op de balans beperken',
	'label_limiter_langues_bilan_nb' => 'Aantal talen',
	'label_nommodule' => 'Naam van de module',
	'label_priorite' => 'Prioriteit',
	'label_proposition_google_translate' => 'Voorstel van Google Translate',
	'label_recherche_module' => 'In module: ',
	'label_recherche_status' => 'Met status: ',
	'label_repertoire_module_langue' => 'Map van de module',
	'label_sauvegarde_locale' => 'Toestaan bestanden lokaal op te slaan',
	'label_sauvegarde_post_edition' => 'Bestanden bij iedere aanpassing opslaan',
	'label_seuil_export_tradlang' => 'Exportdrempel van tradlang (in %)',
	'label_synchro_base_fichier' => 'Database en bestanden synchroniseren',
	'label_texte' => 'Omschrijving van de module',
	'label_tradlang_comm' => 'Commentaar',
	'label_tradlang_status' => 'Status van de vertaling',
	'label_tradlang_str' => 'Vertaalde string (@lang@)',
	'label_update_langues_cible_mere' => 'Deze taal in de database aanpassen',
	'label_valeur_fichier' => 'In jouw bestand',
	'label_valeur_fichier_valider' => 'De aanpassing van jouw bestand valideren',
	'label_valeur_id' => 'Taalcode:',
	'label_valeur_originale' => 'In de database',
	'label_version_originale' => 'Oorspronkelijke string (@lang@)',
	'label_version_originale_choisie' => 'In de gekozen taal (@lang@)',
	'label_version_originale_comm' => 'Commentaar in de oorspronkelijke versie (@lang@)',
	'label_version_selectionnee' => 'String in de gekozen taal (@lang@)',
	'label_version_selectionnee_comm' => 'Commentaar in de gekozen taal (@lang@)',
	'languesdispo' => 'Beschikbare talen',
	'legend_conf_bilan' => 'Balans tonen',
	'legend_descriptifs_priorites' => 'Omschrijving van de prioriteiten',
	'lien_accueil_interface' => 'Beginbladzijde van de vertaalomgeving',
	'lien_aide_recherche' => 'Hulp bij het zoeken',
	'lien_aucun_status' => 'Geen',
	'lien_bilan' => 'Balans van gangbare vertalingen.',
	'lien_check_all' => 'Alles aanklikken',
	'lien_check_none' => 'Niets aanklikken',
	'lien_code_langue' => 'Ongeldige taalcode. De taalcode moet ten minste twee letters bevatten (code ISO-631).',
	'lien_confirm_export' => 'Bevestig de export van het huidige bestand (d.w.z. overschrijven van bestand @fichier@)',
	'lien_editer_chaine' => 'Aanpassen',
	'lien_editer_tous' => 'Pas alle onvertaalde strings aan',
	'lien_export' => 'Het huidige bestand automatisch exporteren.',
	'lien_page_depart' => 'Terugkeren naar de bewaarbladzijde?',
	'lien_profil_auteur' => 'Je profiel',
	'lien_profil_autre' => 'Zijn profiel',
	'lien_proportion' => 'Proportie van getoonde strings',
	'lien_recharger_page' => 'De bladzijde opnieuw laden.',
	'lien_recherche_avancee' => 'Uitgebreid zoeken',
	'lien_retour' => 'Terug',
	'lien_retour_module' => 'Terug naar module "@module@"',
	'lien_retour_page_auteur' => 'Naar je bladzijde terugkeren',
	'lien_retour_page_auteur_autre' => 'Naar zijn bladzijde terugkeren',
	'lien_revenir_traduction' => 'Naar de vertaalbladzijde terugkeren',
	'lien_sauvegarder' => 'Opslaan/Terugzetten van het huidige bestand.',
	'lien_telecharger' => '[Opladen]',
	'lien_toutes_priorite' => 'Alles',
	'lien_traduction_module' => 'Module ',
	'lien_traduction_vers' => ' naar ',
	'lien_traduire_suivant_str_module' => 'De volgende onvertaalde string van module "@module@" vertalen',
	'lien_trier_langue_non' => 'De algemene balans tonen',
	'lien_utiliser_google_translate' => 'Deze versie gebruiken',
	'lien_voir_bilan_lang' => 'De balans bekijken van taal @langue_longue@ (@lang@)',
	'lien_voir_bilan_module' => 'De balans bekijken van module @nom_mod@ - @module@',
	'lien_voir_toute_chaines_module' => 'Alle string van de module bekijken.',

	// M
	'menu_info_interface' => 'Toon een directe link naar de vertaalomgeving',
	'menu_titre_interface' => 'Vertaalomgeving',
	'message_afficher_vos_modules' => 'Modules tonen:',
	'message_aucun_resultat_chaine' => 'Geen enkel resultaat komt overeen met je criteria in de taalstrings.',
	'message_aucun_resultat_statut' => 'Geen enkele string komt overeen met de gevraagde status.',
	'message_aucune_nouvelle_langue_dispo' => 'Deze module is beschikbaar in alle mogelijke talen',
	'message_avertissement_export_langue_complet' => 'Gebruik dit bestand niet in de productie-omgeving. Het is aanbevolen hem slechts in de vertaalomgeving te gebruiken.',
	'message_avertissement_export_langue_po' => 'Dit bestand kan uitsluiten in speciale software worden gebruikt, waarin je hem met de juiste knop importeert.',
	'message_changement_lang_orig' => 'De gekozen brontaal voor de vertaling ("@lang_orig@") is onvoldoende vertaald en vervangen door taal "@lang_nouvelle@".',
	'message_changement_lang_orig_inexistante' => 'De gekozen brontaal voor de vertaling ("@lang_orig@") bestaat niet en is vervangen door taal "@lang_nouvelle@".',
	'message_changement_statut' => 'Aanpassing van status "@statut_old@" naar "@statut_new@"',
	'message_confirm_redirection' => 'Je wordt doorgestuurd naar de aanpassing van de module',
	'message_demande_update_langues_cible_mere' => 'Je kunt een beheerder vragen deze taal opnieuw te synchroniseren met de moedertaal.',
	'message_info_choisir_langues_profiles' => 'Je kunt je voorkeurtalen selecteren <a href="@url_profil@">in je profiel</a> om ze standaard te tonen.',
	'message_lang_cible_selectionnee_auto_preferees' => 'De taal waarnaar je gaat vertalen ("@lang@") werd automatisch gekozen aan de hand van je voorkeurtalen. Je kunt dit aanpassen via het keuzeformulier voor de modules.',
	'message_langues_choisies_affichees' => 'Alleen de door jouw gekozen talen worden getoond: @langues@.',
	'message_langues_preferees_affichees' => 'Alleen jouw voorkeurtalen worden getoond: @langues@.',
	'message_langues_utilisees_affichees' => 'Alleen de @nb@ meest gebruikte talen worden getoond: @langues@.',
	'message_module_langue_ajoutee' => 'De taal "@langue@" werd toegevoegd aan module "@module@".',
	'message_module_updated' => 'De taalmodule "@module@" werd aangepast.',
	'message_passage_trad' => 'We gaan naar het vertalen',
	'message_passage_trad_creation_lang' => 'We maken de taal @lang@ aan en gaan naar het vertalen',
	'message_suppression_module_ok' => 'Module @module@ werd verwijderd.',
	'message_suppression_module_trads_ok' => 'Module @module@ is verwijderd. Ook werden @nb@ bijbehorende vertaalitems verwijderd.',
	'message_synchro_base_fichier_ok' => 'Het bestand en de database zijn gesynchroniseerd.',
	'message_synchro_base_fichier_pas_ok' => 'Het bestand en de database zijn niet gesynchroniseerd.',
	'message_upload_nb_modifie' => 'Je hebt @nb@ taalstrings aangepast.',
	'message_upload_nb_modifies' => 'Je hebt @nb@ taalstrings aangepast.',
	'module_deja_importe' => 'Module "@module@" werd al geïmporteerd',
	'moduletitre' => 'Beschikbare modules',

	// N
	'nb_item_langue_en_trop' => '1 item is overbodig in taal "@langue_longue@" (@langue@).',
	'nb_item_langue_inexistant' => '1 item ontbreekt in taal "@langue_longue@" (@langue@).',
	'nb_item_langue_mere' => 'De brontaal van deze module bevat 1 item.',
	'nb_items_langue_cible' => 'Doeltaal "@langue@" bevat @nb@ in de moedertaal gedefinieerde items.',
	'nb_items_langue_en_trop' => '@nb@ items zijn overbodig in taal "@langue_longue@" (@langue@).',
	'nb_items_langue_inexistants' => '@nb@ items ontbreken in taal "@langue_longue@" (@langue@).',
	'nb_items_langue_mere' => 'De brontaal van deze module bevat @nb@ items.',
	'notice_affichage_limite' => 'Slechts @nb@ onvertaalde taalstrings worden getoond.',
	'notice_aucun_module_favori_priorite' => 'Geen enkele module met prioriteit "@priorite@" komt overeen.',

	// R
	'readme' => 'Met deze plugin kunnen taalbestanden worden beheerd',

	// S
	'str_status_modif' => 'Aangepast',
	'str_status_new' => 'Nieuw',
	'str_status_relire' => 'Te herzien',
	'str_status_traduit' => 'Vertaald',

	// T
	'texte_contacter_admin' => 'Neem contact op met een beheerder wanneer je wilt deelnemen.',
	'texte_erreur' => 'FOUT',
	'texte_erreur_acces' => '<b>Let op: </b>kan niet schrijven in bestand <tt>@fichier_lang@</tt>. Controleer de toegangsrechten.',
	'texte_existe_deja' => ' bestaat al.',
	'texte_explication_langue_cible' => 'Voor de doeltaal moet je kiezen of je aan een bestaande vertaling wilt werken of dat je een nieuwe taal wilt aanmaken.',
	'texte_export_impossible' => 'Het bestand kan niet worden geëxporteerd. Controleer de schrijfrechten op bestand @cible@',
	'texte_filtre' => 'Filteren (zoeken)',
	'texte_inscription_ou_login' => 'Je moet een account op de site aanmaken of je identificeren om te kunnen vertalen.',
	'texte_interface' => 'Vertaalomgeving: ',
	'texte_interface2' => 'Vertaalomgeving',
	'texte_langue' => 'Taal:',
	'texte_langue_cible' => 'de doeltaal is de taal waarin je vertaalt;',
	'texte_langue_origine' => 'de brontaal dient als model (waarbij de moedertaal de voorkeur heeft);',
	'texte_langues_differentes' => 'De doeltaal en de brontaal moeten verschillend zijn.',
	'texte_modifier' => 'Aanpassen',
	'texte_module' => 'de te vertalen taalmodule;',
	'texte_module_traduire' => 'De te vertalen module:',
	'texte_non_traduit' => 'onvertaald ',
	'texte_operation_impossible' => 'Onmogelijke handeling. Wanneer je ’alles selecteren’ hebt aangekruist,<br> moet je handelingen doen van het type ’Raadplegen’.',
	'texte_pas_autoriser_traduire' => 'Je hebt niet de nodige rechten om bij de vertalingen te komen.',
	'texte_pas_de_reponse' => '... geen enkele respons',
	'texte_recapitulatif' => 'Samenvatting vertalingen',
	'texte_restauration_impossible' => 'onmogelijk het bestand terug te zetten',
	'texte_sauvegarde' => 'Vertaalomgeving, Opslaan/Terugzetten van bestand',
	'texte_sauvegarde_courant' => 'Opgeslagen kopie van het huidige bestand:',
	'texte_sauvegarde_impossible' => 'onmogelijk het bestand op te slaan ',
	'texte_sauvegarder' => 'Opslaan',
	'texte_selection_langue' => 'Om een (gedeeltelijk) vertaald taalbestand te tonen, moet je de taal kiezen: ',
	'texte_selectionner' => 'Om met het vertaalwerk te beginnen, moet je kiezen:',
	'texte_selectionner_version' => 'Kies de versie van het bestand en klik daarna op de knop ernaast.',
	'texte_seul_admin' => 'Alleen een beheerder heeft toegang tot deze stap.',
	'texte_total_chaine' => 'Aantal strings:',
	'texte_total_chaine_conflit' => 'Aantal meest gebruikte strings:',
	'texte_total_chaine_modifie' => 'Aantal aan te passen strings:',
	'texte_total_chaine_non_traduite' => 'Aantal onvertaalde strings:',
	'texte_total_chaine_traduite' => 'Aantal vertaade strings:',
	'texte_tout_selectionner' => 'Alles selecteren',
	'texte_type_operation' => 'Type handeling',
	'texte_voir_bilan' => 'Bekijk de <a href="@url@" class="spip_in">balans van vertalingen</a>.',
	'tfoot_total' => 'Totaal',
	'th_avancement' => 'Vooruitgang',
	'th_comm' => 'Commentaar',
	'th_date' => 'Datum',
	'th_items_modifs' => 'Aangepaste items',
	'th_items_new' => 'Nieuwe items',
	'th_items_relire' => 'Te herziene items',
	'th_items_traduits' => 'Vertaalde items',
	'th_langue' => 'Taal',
	'th_langue_mere' => 'Moedertaal',
	'th_langue_origine' => 'Tekst in oorspronkelijke taal',
	'th_langue_voulue' => 'Vertaling in het "@lang@"',
	'th_module' => 'Module',
	'th_status' => 'Status',
	'th_total_items_module' => 'Totaal aantal items',
	'th_traduction' => 'Vertaling',
	'th_traduction_voulue' => 'Vertaling in het "@lang@"',
	'tire_recherche_modules_favoris' => 'Modules zoeken',
	'titre_bilan' => 'Balans van vertalingen',
	'titre_bilan_langue' => 'Balans van vertalingen in taal "@lang@"',
	'titre_bilan_module' => 'Balans van vertalingen van module "@module@"',
	'titre_changer_langue_selection' => 'De gekozen taal wijzigen',
	'titre_changer_langues_affichees' => 'De getoonde talen wijzigen',
	'titre_commentaires_chaines' => 'Commentaar op het onderwerp van deze string',
	'titre_commenter_chaine' => 'Commentaar op deze string leveren',
	'titre_distributions' => 'De distributies',
	'titre_form_import_step_1' => 'Stap 1: versturen van je bestand',
	'titre_form_import_step_2' => 'Stap 2: controleren van je aanpassingen',
	'titre_inscription' => 'Inschrijving',
	'titre_logo_tradlang_module' => 'Logo van de  module',
	'titre_modifications_chaine_originale' => 'Van de originele string',
	'titre_modifications_chaine_traduite' => 'Van deze string',
	'titre_modifications_chaines' => 'De laatste aanpassingen',
	'titre_modifier' => 'Aanpassen',
	'titre_page_auteurs' => 'Lijst van medewerkers',
	'titre_page_configurer_tradlang' => 'Configuratie van plugin Trad-lang',
	'titre_page_tradlang_module' => 'Module #@id@: @module@',
	'titre_profil_auteur' => 'Je profiel aanpassen',
	'titre_profil_autre' => 'Pas zijn profiel aan',
	'titre_recherche_tradlang' => 'Taalstrings',
	'titre_revisions_ses' => 'Zijn bijdragen',
	'titre_revisions_sommaire' => 'Laatste aanpassingen',
	'titre_revisions_vos' => 'Jouw bijdragen',
	'titre_stats_ses' => 'Zijn statistieken',
	'titre_stats_trads_journalieres' => 'Aantal dagelijkse revisies',
	'titre_stats_trads_mensuelles' => 'Aantal maandelijke revisies',
	'titre_stats_vos' => 'Je statistieken',
	'titre_tradlang' => 'Trad-lang',
	'titre_tradlang_chaines' => 'Taalstrings',
	'titre_tradlang_export' => 'Exporteer module "@module@" in het "@lang_longue@ (@lang@)"',
	'titre_tradlang_export_traduire' => 'Voor lokale vertaling',
	'titre_tradlang_export_utiliser' => 'Voor gebruik in productie',
	'titre_tradlang_module' => 'Taalmodule',
	'titre_tradlang_modules' => 'Taalmodules',
	'titre_tradlang_non_traduit' => '1 onvertaalde taalstring',
	'titre_tradlang_non_traduits' => '@nb@ onvertaalde taalstrings',
	'titre_traduction' => 'Vertalingen',
	'titre_traduction_chaine_de_vers' => 'Vertaling van string « @chaine@ » van module « @module@ » van het <abbr title="@lang_orig_long@">@lang_orig@</abbr> naar het <abbr title="@lang_cible_long@">@lang_cible@</abbr>',
	'titre_traduction_de' => 'Vertaling van ',
	'titre_traduction_module_de_vers' => 'Vertaling van module "@module@" van het <abbr title="@lang_orig_long@">@lang_orig@</abbr> naar het <abbr title="@lang_cible_long@">@lang_cible@</abbr>',
	'titre_traduire' => 'Vertalen',
	'tradlang' => 'Trad-Lang',
	'traduction' => 'Vertaling @lang@',
	'traductions' => 'Vertalingen'
);
