<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/statistiques');


include_spip('inc/statistiques');

/**
 * Retourne les statistiques de versionnage des traductions pour une durée donnée
 *
 * @param string $unite jour | mois | annee
 * @param ?int $duree Combien de jours | mois | annee on prend…
 * @param ?string $objet (unused)
 * @param ?int $id_objet (id_auteur)
 * @return array [date => nb visites]
 */
function inc_stats_trads_to_array_dist($unite, ?int $duree = null, ?string $objet = null, ?int $id_objet = null) {

	$unites = [
		'jour' => 'day',
		'day' => 'day',
		'semaine' => 'week',
		'week' => 'week',
		'mois' => 'month',
		'month' => 'month',
		'annee' => 'year',
		'year' => 'year',
	];
	$unite = $unites[$unite] ?? 'day';
	$period_unit = $unite;
	$period_duration = $duree;

	switch ($unite) {
		case 'day':
			$format_sql = '%Y-%m-%d';
			$format = 'Y-m-d';
			$period_unit_interval = 'D';
			break;

		case 'week':
			// https://en.wikipedia.org/wiki/ISO_week_date
			$format_sql = '%x-W%v';
			$format = 'o-\WW';
			$n_today = (new \DateTime())->format('w'); // dimanche 0, samedi 6
			// on se cale sur un lundi
			$period_duration = 7 * $duree - $n_today;
			$period_unit = 'day';
			$period_unit_interval = 'D';
			break;

		case 'month':
			$format_sql = '%Y-%m';
			$format = 'Y-m';
			$period_unit_interval = 'M';
			break;

		case 'year':
			$format_sql = '%Y';
			$format = 'Y';
			$period_unit_interval = 'Y';
			break;

		default:
			throw new \RuntimeException("Invalid unit $unite");
	}

	if ($duree and $duree < 0) {
		$duree = null;
	}

	$serveur = '';
	$table = 'spip_versions';
	$order = 'date';

	$objet = 'tradlang';
	$id_auteur = $id_objet;

	$where = [
		'objet = ' . sql_quote($objet),
		'id_version > 0'
	];
	if (!$id_auteur) {
		$where[] = 'id_auteur > 0';
	} else {
		$where[] = 'id_auteur = ' . $id_auteur;
	}

	$currentDate = (new \DateTime())->format($format);
	$startDate = null;
	$endDate = $currentDate;

	if ($duree) {
		$where[] = sql_date_proche($order, -$period_duration, $period_unit, $serveur);
		// sql_date_proche utilise une comparaison stricte. On soustrait 1 jour...
		$startDate = (new \DateTime())->sub(new \DateInterval('P' . ($period_duration - 1) . $period_unit_interval))->format($format);
	}

	$where = implode(' AND ', $where);

	$firstDateStat = sql_getfetsel('date', $table, $where, '', 'date', '0,1');
	if ($firstDateStat) {
		$firstDate = (new \DateTime($firstDateStat))->format($format);
	} else {
		$firstDate = null;
	}

	$data = sql_allfetsel(
		"DATE_FORMAT($order, '$format_sql') AS formatted_date, COUNT(*) AS visites",
		$table,
		$where,
		'formatted_date',
		'formatted_date',
		'',
		'',
		$serveur
	);

	$data = array_map(function ($d) {
		$d['date'] = $d['formatted_date'];
		unset($d['formatted_date']);
		return $d;
	}, $data);

	return [
		'meta' => [
			'unite' => $unite,
			'duree' => $duree,
			'id_auteur' => $id_auteur,
			'format_date' => $format,
			'start_date' => $startDate ?? $firstDate ?? $endDate,
			'end_date' => $endDate,
			'first_date' => $firstDate,
			'columns' => [
				'date',
				'visites',
			],
			'translations' => [
				'date' => _T('public:date'),
				'visites' => label_nettoyer(_T('tradlang:info_revisions_stats')),
				'moyenne' => _T('statistiques:moyenne'),
			],
		],
		'data' => array_values($data),
	];
}

